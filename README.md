# HZTL Boilerplate 2021

## Developer Workstation Prerequisites

For local development, the following are required prior to running the project:

- [Node](https://nodejs.org/) (>=12.0.0)
- [Gulp](https://gulpjs.com/) (>=4.0.0)
- [ESLint](https://eslint.org/) (>=7.11.0)

Installation instructions for each can be found on their respective websites.

## Check Out Project and Install Dependencies

$ git clone git@bitbucket.org:horizontal/hztl-open-container-pages.git
$ cd <YOUR PROJECT FOLDER>
$ npm install

## Local Development

$ cd <YOUR PROJECT FOLDER>

$ npm run dev

## Build Icon Font

$ cd <YOUR PROJECT FOLDER>

$ gulp icons

## Build for Production

$ cd <YOUR PROJECT FOLDER>

$ gulp prod

---

## Code Standards

- [Horizontal Digital HTML and CSS Style Guide](https://github.com/horizontalintegration/css-standards)
- [Horizontal Digital JavaScript Style Guide](https://github.com/horizontalintegration/javascript-standards)

## Languages & Tools

### Web Services

- [Express](https://expressjs.com/) for handling web requests.

### JavaScript

- [React](http://facebook.github.io/react) is used for UI.

### CSS

- [SASS](https://sass-lang.com) is used to write futureproof CSS.
