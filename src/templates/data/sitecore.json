{
  "title": "Sitecore | Horizontal Digital",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title":"Sitecore | Horizontal Digital",
    "description":"An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url":"https://horizontaldigital.com/platforms",
    "sitename":"Horizontal Digital",
    "img":{
      "src":"https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template":"components/section-wide-media",
      "class":"collapse-bottom-margin",
      "imgs":[
        {
          "src":"/assets/images/platforms/SitecoreHero.png",
          "alt":"Sitecore"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"We immerse ourselves in Sitecore to take experiences further",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>We proudly take a proactive approach to learning about Sitecore’s capabilities, offerings and future opportunities — a mindset that we’ve shared since opening our doors in 2004. This commitment allows us to imagine and implement experiences that truly meet customer needs in the moment and elevate our client’s business goals in the process.</p>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"We’re proud to be an original Sitecore Platinum Partner",
      "content": [
        {
          "template":"components/component-data-tile",
          "items":[
            {"content":"<div class=\"display-1\">14 MVPs</div>"},
            {"content":"<div class=\"display-1\">70</div><div class=\"display-4\">Certified Developers</div><p>Highest concentration of Sitecore developers in one agency</p>"},
            {"content":"<div class=\"display-1\">100+</div><div class=\"display-4\">Implementations</div>"}
          ]
        },
        {
          "template":"components/component-media",
          "imgs":[
            {
              "src":"/assets/images/platforms/sitecore/Sitecore-Badge-images.png",
              "alt":"Sitecore Badges"
            }
          ]
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"Sitecore platform technologies",
      "content": [
        {
          "template":"components/component-cards",
          "items": [
            {
              "title":"Sitecore Experience Platform - XP",
              "img":{
                "src":"/assets/images/platforms/sitecore/Sitecore-ExpPlatform.png",
                "alt":"Marketing Cloud"
              },
              "link":{
                "url":"/platforms/sitecore/sc-exp-platform",
                "text":"Learn more"
              }
            },
            {
              "title":"Sitecore Experience Commerce - XC",
              "img":{
                "src":"/assets/images/platforms/sitecore/Sitecore-ExpCommerce.png",
                "alt":"Marketing Cloud"
              },
              "link":{
                "url":"/platforms/sitecore/sc-exp-commerce",
                "text":"Learn more"
              }
            },
            {
              "title":"Sitecore Content Hub",
              "img":{
                "src":"/assets/images/platforms/sitecore/Sitecore-ContentHub.png",
                "alt":"Marketing Cloud"
              },
              "link":{
                "url":"/platforms/sitecore/sc-content-hub",
                "text":"Learn more"
              }
            }
          ]
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"collapse-bottom-margin",
      "title":"Anticipation is our everything",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Personalization. Predictive content. Contextual experiences. These are the foundations of successful customer engagements. We harness Sitecore’s capabilities to help clients stay in sync with ever-evolving customer expectations and build lasting relationships in the process.</p>"
        }
      ]
    },
    {
      "template":"components/section-wide-media",
      "class":"medium-bottom-margin",
      "video":{
        "poster":{
          "src":"/assets/images/platforms/sitecore/amy-gath-still.jpeg",
          "alt":"Salesforce Video"
        },
        "id":"413243418"
      }
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"Our team",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Many experts with one mission: Create Sitecore experiences that continuously elevate expectations.​</p>"
        },
        {
          "template":"components/component-team",
          "title":"Technical",
          "items": [
            {
              "name":"Sheetal Jain",
              "title":"Sitecore Practice Lead",
              "linkedin":"https://www.linkedin.com/in/sheetal-k-jain/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/SheetalJain.png"
              }
            },
            {
              "name":"Rama Ila",
              "title":"Senior Technologist & Sitecore MVP",
              "linkedin":"https://www.linkedin.com/in/rama-krishna-ila-80853739/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/Seafoam.png"
              }
            },
            {
              "name":"Kiran Patil",
              "title":"Senior Technology Lead",
              "linkedin":"https://www.linkedin.com/in/kiranpatils/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/KiranPatil.png"
              }
            }
          ]
        },
        {
          "template":"components/component-team",
          "title":"Strategy",
          "items": [
            {
              "name":"Chris Staley",
              "title":"Co-Founder & SVP",
              "linkedin":"https://www.linkedin.com/in/chris-staley-26492b2/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/ChrisUpdated.png"
              }
            },
            {
              "name":"Dave Michela",
              "title":"VP, Digital Solutions",
              "linkedin":"https://www.linkedin.com/in/davemichela/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/DaveUpdated.png"
              }
            },
            {
              "name":"Jason Estes",
              "title":"Regional Managing Director",
              "linkedin":"https://www.linkedin.com/in/jasoncestes/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/JasonUpdated.png"
              }
            }
          ]
        },
        {
          "template":"components/component-team",
          "title":"Ambassadors",
          "items": [
            {
              "name":"Sabin Ephrem",
              "title":"Co-Founder & CEO",
              "linkedin":"https://www.linkedin.com/in/sabinephrem/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/SabinEphrem.png"
              }
            },
            {
              "name":"Mitch Talbot",
              "title":"Business Development Director",
              "linkedin":"https://www.linkedin.com/in/mitch-talbot-43427b3/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/MitchTalbot.png"
              }
            },
            {
              "name":"George Smith",
              "title":"Regional Managing Director",
              "linkedin":"https://www.linkedin.com/in/georgeksmith/",
              "img":{
                "src":"/assets/images/platforms/sitecore/team/GeorgeSmith.png"
              }
            }
          ]
        }
      ]
    }
  ]
}
