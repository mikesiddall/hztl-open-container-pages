{
  "title": "Customer Data Management | Horizontal Digital",
  "theme": "theme-dark",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title": "Customer Data Management | Horizontal Digital",
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url": "https://horizontaldigital.com/services",
    "sitename": "Horizontal Digital",
    "img": {
      "src": "https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template": "components/section-banner",
      "class": "collapse-bottom-margin",
      "col":"col-md-8",
      "headline": {
        "title": "Marketing Automation",
        "size": "display-xlarge display-super"
      },
      "img": "https://hiprodcdn.azureedge.net/-/media/project/horizontal/dotcom/digital/cxstrategyhero_2x.png",
      "alt": " "
    },
    {
      "template": "components/section-col-single",
      "title": "The right message at every opportunity",
      "content": [
        {
          "template": "components/rte",
          "content": "<p>The way people engage with companies and brands is often unpredictable. Awareness might happen a split-second before purchase. Advocacy might occur without any purchase intent whatsoever. Mix in every variable and you have a 25,000-piece puzzle when it comes to making a meaningful connection with your customers.</p><p>Our marketing automation expertise powers successful communications programs across email, SMS notifications, social, blogs and other content vehicles. The result: Personalized messages at scale for unique users, without the need to churn marketing team hours and intervention to ensure effective outcomes.</p><p>But we don’t stop there. Through our connected experience focus, we help organizations use their data from interactions across their digital platforms to provide an in-depth, 360-degree view of your customers. And with this intelligence, we activate marketing messages that anticipate customer needs vs. simply reacting to them.</p>"
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "collapse-bottom-margin",
      "title": "Our capabilities and expertise",
      "content": [
        {
          "template": "components/rte",
          "content": "<h2 class=\"display-3\">Strategy</h2>"
        },
        {
          "template": "components/component-cards-see-more",
          "items": [
            {
              "title": "Platform selection",
              "list": [
                "B2B/B2C",
                "Objective to use case mapping",
                "Long term organizational impact",
                "System sunsetting",
                "KPI identification"
              ]
            },
            {
              "title": "Architecture",
              "list": [
                "Data segementation",
                "Integration",
                "Migration planning",
                "Business unit strategy"
              ]
            },
            {
              "title": "Journey definition",
              "list": [
                "Personal definition",
                "Outcome definitions",
                "Experience map",
                "Key behavior identification"
              ]
            },
            {
              "title": "Omni-channel design",
              "list": [
                "Campaign structure",
                "SMS",
                "Cross-channel orchestration",
                "Ads",
                "Email"
              ]
            },
            {
              "title": "Lead management",
              "list": [
                "Nurture strategy",
                "Segmentation",
                "Scoring",
                "Top of the funnel"
              ]
            },
            {
              "title": "Content",
              "list": ["Dynamic", "Creative", "Localized", "Personalized"]
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "collapse-bottom-margin",
      "content": [
        {
          "template": "components/rte",
          "content": "<h2 class=\"display-3\">Development</h2>"
        },
        {
          "template": "components/component-cards-see-more",
          "items": [
            {
              "title": "Email platform implementation",
              "list": [
                "New business unit set up",
                "CRM integration",
                "Email template creation",
                "Journey building"
              ]
            },
            {
              "title": "Social listening",
              "list": [
                "Sentiment identification",
                "Key word targeting",
                "Social customer service",
                "Competitive analysis"
              ]
            },
            {
              "title": "Preference center",
              "list": [
                "Subscription management",
                "Interest capture",
                "Compliance",
                "Opt in / Opt down / Opt out"
              ]
            },
            {
              "title": "Landing pages",
              "list": ["Forms", "Data capture", "Lead prcoess integration"]
            },
            {
              "title": "IP reputation management",
              "list": [
                "Reputation assessment",
                "IP warning",
                "Safe list strategy",
                "Deliverability best practice"
              ]
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "medium-bottom-margin",
      "content": [
        {
          "template": "components/rte",
          "content": "<h2 class=\"display-3\">Optimization & operations</h2>"
        },
        {
          "template": "components/component-cards-see-more",
          "items": [
            {
              "title": "Marketing governance",
              "list": [
                "Data upkeep",
                "Risk mitigation",
                "Security model",
                "Approval process",
                "Channel strategy"
              ]
            },
            {
              "title": "Marketing support services",
              "list": [
                "Campaign support",
                "Crewative services",
                "Roadmap strategy"
              ]
            },
            {
              "title": "Analytics & AI",
              "list": [
                "Send tiem optimization",
                "Propensity identification",
                "Content testing",
                "Connected marketing intelligence",
                "KP measurement"
              ]
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "medium-bottom-margin",
      "title": "Platforms & technologies",
      "content": [
        {
          "template": "components/component-cards",
          "items": [
            {
              "title": "Sitecore",
              "content": "<ul class=\"list-unstyled\"><li><a href=\"/platforms/sitecore/sc-exp-platform\">Sitecore Experience Platform</a></li><li><a href=\"/platforms/sitecore/sc-exp-commerce\">Sitecore Experience Commerce</a></li><li><a href=\"/platforms/sitecore/sc-content-hub\">Sitecore Content Hub</a></li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/SitecoreIcon.jpeg",
                "alt": "Sitecore"
              },
              "link": {
                "url": "/platforms/sitecore/",
                "text": "Learn more"
              }
            },
            {
              "title": "Salesforce",
              "content": "<ul class=\"list-unstyled\"><li><a href=\"/platforms/salesforce/marketing-cloud\">Pardot</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Marketing Cloud</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Journey Builder</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Social Studio</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Email Studio</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Audience Studio</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Interaction Studio</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Mobile Studio</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Datorama</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Customer 360 Audiences (CDP)</a></li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/SalesforceIcon.jpeg",
                "alt": "Salesforce"
              },
              "link": {
                "url": "/platforms/salesforce/",
                "text": "Learn more"
              }
            },
            {
              "title": "Acquia",
              "content": "<ul class=\"list-unstyled\"><li>Acquia Personalization</li><li>Acquia Campaign Studio</li><li>Acquia Campaign Factory</li><li>Acquia CDP</li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/AcquiaIcon.jpeg",
                "alt": "Acquia"
              },
              "link": {
                "url": "/platforms/acquia/",
                "text": "Learn more"
              }
            },
            {
              "title": "Coveo",
              "content": "<ul class=\"list-unstyled\"><li>Coveo Recommender</li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/CoveoIcon.jpeg",
                "alt": "Coveo"
              },
              "link": {
                "url": "/platforms/coveo/",
                "text": "Learn more"
              }
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "title": "See how we ensure long term success",
      "content": [
        {
          "template": "components/rte",
          "content": "<h1>TODO data tile?</h1>"
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "title": "Other Services",
      "content": [
        {
          "template": "components/rte",
          "content": "<h1>TODO</h1>"
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "content": [
        {
          "template": "components/rte",
          "content": "<h1>TODO</h1>"
        }
      ]
    }
  ]
}
