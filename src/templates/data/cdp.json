{
  "title": "Salesforce CDP | Horizontal Digital",
  "theme":"theme-dark-header",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title":"Salesforce Interaction Studio | Horizontal Digital",
    "description":"An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url":"https://horizontaldigital.com/platforms",
    "sitename":"Horizontal Digital",
    "img":{
      "src":"https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template":"components/section-banner",
      "class":"theme-dark flush-bottom-margin",
      "col":"col-md-8",
      "headline":{
        "title": "CDP",
        "size":"display-large",
        "super":"Salesforce"
      }
    },
    {
      "template": "components/section-wide-media",
      "class":"theme-dark-top collapse-bottom-margin",
      "imgs": [
        {
          "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/CDP-360-page.jpg",
          "alt": "CDP hero image"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"collapse-bottom-margin",
      "title":"Our Salesforce CDP expertise drives actionable outcomes from your data",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>We believe connected experiences drive greater possibilities between brands and their customers. And that starts by having connected data. But in order to have connected data, it’s imperative to ensure the systems themselves are connected into one centralized dashboard.</p><p>That’s where Salesforce CDP comes in.</p><p>Not only is data across Salesforce clouds aggregated into the customer data platform, but it also ingests data from external systems with its native connectors to truly provide a unified. This combined with our deep expertise in data management as well as our philosophy of always putting the customer at the center of everything we do is what builds seamless customer journeys with no frictions, no loopholes, and no dead ends.</p><p>This Experience-Forward mindset is simply the best way to turn your data into actionable outcomes that drives results. </p>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"collapse-bottom-margin",
      "title":"We unify your data for a single source of truth",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Most enterprise companies are sitting on a wealth of customer data, but more often than not...swimming in analysis paralysis . The issue is disconnected teams and departments collect different customer data points across multiple channels, touchpoints, and devices. Our expertise is in stitching these data points together through the power of Salesforce CDP, helping brands achieve a single source of truth offor their customer records. This centralized database provides intelligence that identifies new and existing users to deliver the right experiences at the right time on the right channel.</p>"
        },
        {
          "template":"components/component-data-tile",
          "items":[
            {"content":"<div class=\"display-1\">73%</div><div class=\"display-4\">of companies</div><p> report that a CDP will be critical to their customer experience efforts</p>"},
            {"content":"<div class=\"display-1\">80%</div><div class=\"display-4\">average revenue</div><p> increase for businesses that focus on improving customer experience</p>"},
            {"content":"<div class=\"display-1\">62%</div><div class=\"display-4\">of US retailers</div><p> have over 50 systems housing customer data</p>"}
          ]
        }
      ]
    },
    {
      "template":"components/section-col-half",
      "class":"theme-lite medium-bottom-margin",
      "cols":{
        "left":"col-md-6",
        "right":"col-md-6"
      },
      "title":"Our Experience-Forward approach to delivering long-term data management success",
      "img":{
        "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/ProcessVennDiagram.png",
        "alt":"End-to-end experience diagram"
      },
      "content": {
        "left": [
          {
            "template": "components/rte",
            "content": "<p>We understand that data is just the beginning. Our unique approach of fusing our certified Salesforce CDP specialists with our team of CX strategists and UX designers is what enables our clients to experience fully orchestrated, unified data that empowers the creation of personalized customer journeys at scale.</p><p>Throughout the process, we work with your team to ensure the right data is gathered and systems are put in place to keep it clean, protected, consistent and actionable. Plus, we ensure all Salesforce implementations are right the first time around, built for future scalability, and supported with training and enablement for customer independence and long-term success. </p>"
          }
        ]
      }
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"The Salesforce CDP advantage",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Salesforce CDP allows marketers to unify all their data – from anywhere – for a complete view of their customer. The purpose-built software solves the issues so many marketers face of having fragmented data spread across a myriad of tools by ingesting data from all the Salesforce clouds as well as from external sources outside the Salesforce platform. This unified data gives you a complete view of your customer, allowing you to deliver each customer with personalized content on a 1:1 level that drives them to their next best action.</p>"
        },
        {
          "template":"components/component-wide-tile",
          "items":[
            {"content":"<div class=\"display-3\">1. Integration & intelligence</div><p>Not only is Salesforce CDP fully equipped with its native connectors to their data powerhouse platforms like Tableau, MuleSoft, and Datorama, but it also has integrations with external data sources to connect all your data points across your systems into one centralized platform. This wealth of data is then backed by the power of Einstein to create intelligent segments that deliver personalized content on the right channel at the right moment.</p>"},
            {"content":"<div class=\"display-3\">2. Easy drag-and-drop interface</div><p>Marketers can leverage Salesforce CDP to build smarter audience segments from one user-friendly, drag-and-drop interface. Plus, with their built-in AI capabilities, you spend less time analyzing your data and more time testing and optimizing your targeting efforts on the fly.</p>"},
            {"content":"<div class=\"display-3\">3. Built-in compliance</div><p>Salesforce CDP is built to handle data and consumer rights to ensure every experience you create is compliant with GDPR and CCPA. The platform also includes safe provisioning measures and dashboards for opt-outs and consent flags, and is created with future flexibility to scale with any new data legislation.</p>"}
          ]
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"Take compliance & preferences to the next level with NCPC",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Our No Code Preference Center custom application ensures you’re always in compliance with consumer rights and data policy. Plus, with the removal of third-party cookies, it gives you an out-of-the-box solution to capture expressed customer preferences using first-party cookies.</p><div><a href=\"https://www.horizontaldigital.com/en/platforms/salesforce/email-preference-center/\" class=\"btn btn-cta\">Learn More <i class=\"fas fa-arrow-right\"></i></a></div>"
        }
      ]
    },
    {
      "template":"components/section-col-half",
      "class":"medium-bottom-margin",
      "cols":{
        "left":"col-md-6",
        "right":"col-md-6"
      },
      "title":"Salesforce CDP is an integral part of Marketing Cloud",
      "img":{
        "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/salesforce/sf_marketingcloud_logo.png",
        "alt":"End-to-end experience diagram"
      },
      "content": {
        "left": [
          {
            "template": "components/rte",
            "content": "<p>As a Level II Specialized Marketing Cloud expert, we harness the power of the platform to cement stronger customer relationships. With more than 47 marketing certifications and counting, our Marketing Cloud expertise powers meaningful customer moments at every opportunity. And with Salesforce CDP under the Marketing Cloud umbrella, our team of experts seamlessly ties the two together.</p><div><a href=\"https://www.horizontaldigital.com/platforms/salesforce/marketing-cloud/\" class=\"btn btn-cta\">Learn More <i class=\"fas fa-arrow-right\"></i></a></div>"
          }
        ]
      }
    },
    {
      "template":"components/section-col-single",
      "title":"Contact our Salesforce CDP team",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Want to learn more about unifying your data with Salesforce CDP? Our team of experts is ready to answer any questions and talk through your goals.</p><div><a href=\"https://www.horizontaldigital.com/contact-us/\" class=\"btn btn-cta\">Contact Us <i class=\"fas fa-arrow-right\"></i></a></div>"
        }
      ]
    },
    {
      "template":"components/section-footer-cta",
      "items": [
        {
          "type":"insights",
          "tag":"Insights",
          "title":"The power of personalization",
          "content":"Five ways to harness the power of website personalization.",
          "link":{
            "url":"https://www.horizontaldigital.com/insights/5-ways-to-harness-the-power-of-website-personalization"
          }
        },
        {
          "type":"work",
          "tag":"Work",
          "title":"Daltile",
          "content":"Crafting personalized customer journeys for a titan of tile.",
          "link":{
            "url":"https://www.horizontaldigital.com/work/daltile"
          }
        }
      ]
    }
  ]
}
