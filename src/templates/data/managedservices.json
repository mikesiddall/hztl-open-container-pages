{
  "title": "Customer Data Management | Horizontal Digital",
  "theme": "theme-dark",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title": "Customer Data Management | Horizontal Digital",
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url": "https://horizontaldigital.com/services",
    "sitename": "Horizontal Digital",
    "img": {
      "src": "https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template": "components/section-banner",
      "class": "collapse-bottom-margin",
      "col":"col-md-8",
      "headline": {
        "title": "Managed Services",
        "size": "display-xlarge display-super"
      },
      "img": {
        "src": "https://hiprodcdn.azureedge.net/-/media/project/horizontal/dotcom/digital/datascience_hero_2x.png?h=1120&w=2480&revision=fcf96916-6398-4514-9311-031cbd72ae9c&modified=20201026223728&hash=30405DA0176085C67B475DBCC31C40C1",
        "alt": " "
      }
    },
    {
      "template": "components/section-col-single",
      "class": "collapse-bottom-margin",
      "title": "Solutions that adapt to your needs",
      "content": [
        {
          "template": "components/rte",
          "content": "<p>Transformative digital experiences require constant care and nurturing. We provide fully customizable programs empowering our clients to decide what they want to manage themselves and what they prefer to outsource. In addition, our solutions include retainer-based teams covering all digital skill sets, and subscription-based managed services for 24/7/365 technology maintenance and incident management.</p><p>Clients who prefer to self-manage but who need a little extra horsepower from time to time work with our sister business, Horizontal Talent, to procure top-flight digital expertise on a contract, contract-to-hire or direct placement basis.</p>"
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "medium-bottom-margin",
      "title": "Our capabilities and expertise",
      "content": [
        {
          "template": "components/component-cards-see-more",
          "items": [
            {
              "title": "Application & infrastructure managed services",
              "list": [
                "Tier 1/Tier 2 incident management",
                "24/7/365 SLAs",
                "User provisioning and management",
                "Tier 1 customizations",
                "Data loading and cleansing"
              ]
            },
            {
              "title": "DevOps & CI/CD optimization",
              "list": ["Cloud optimization", "SLDC automation & optimization"]
            },
            {
              "title": "Application maintenance support",
              "list": ["Agile backlog management", "Dedicated or shared teams"]
            },
            {
              "title": "Organizational change management",
              "list": [
                "Digital operations optimization",
                "Digital governance & workflow optimization"
              ]
            },
            {
              "title": "Training & upskilling",
              "list": [
                "Technologiest training",
                "Marketer/content manager training"
              ]
            },
            {
              "title": "SEO",
              "list": [
                "SEO strategy & tactical planning",
                "Ongoing analysis & optimization"
              ]
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "medium-bottom-margin",
      "title": "Platforms & technologies",
      "content": [
        {
          "template": "components/component-cards",
          "items": [
            {
              "title": "Sitecore",
              "content": "<ul class=\"list-unstyled\"><li><a href=\"/platforms/sitecore/sc-exp-platform\">Sitecore Experience Platform</a></li><li><a href=\"/platforms/sitecore/sc-exp-commerce\">Sitecore Experience Commerce</a></li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/SitecoreIcon.jpeg",
                "alt": "Sitecore"
              },
              "link": {
                "url": "/platforms/sitecore/",
                "text": "Learn more"
              }
            },
            {
              "title": "Salesforce",
              "content": "<ul class=\"list-unstyled\"><li><a href=\"/platforms/salesforce\">Service Cloud</a></li><li><a href=\"/platforms/salesforce\">Sales Cloud</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Pardot</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Marketing Cloud</a></li><li><a href=\"/platforms/salesforce/analytics-ai\">Tableau CRM</a></li><li><a href=\"/platforms/salesforce/marketing-cloud\">Datorama</a></li></ul>",
              "img": {
                "src": "https://hztl-fed.azureedge.net/hztl/images/platforms/SalesforceIcon.jpeg",
                "alt": "Salesforce"
              },
              "link": {
                "url": "/platforms/salesforce/",
                "text": "Learn more"
              }
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-single",
      "class": "collapse-bottom-margin",
      "title": "See how we help clients think bigger"
    },
    {
      "template": "components/component-cards-case-study",
      "img": {
        "src": "https://hiprodcdn.azureedge.net/-/media/project/horizontal/dotcom/digital/b2-case-studies/small-case-studies/hztl_small_casestudy_images_cw.jpg?revision=c8597d32-0adc-4ca6-a771-00b2d99d725a&modified=20201025220831&w=300&hash=E67C99407BAB82DD716EF7B37B8BF224",
        "alt": " "
      },
      "title": "Cushman & Wakefield",
      "link": {
        "src": "https://www.horizontaldigital.com/work/cushman-wakefield",
        "alt": " ",
        "text": "Read case study"
      }
    },
    {
      "template": "components/section-col-single",
      "title": "Other Services",
      "content": [
        {
          "template": "components/component-link-list",
          "items": [
            {
              "link": {
                "url": "/services/cx-strategy-and-design",
                "text": "CX Strategy & Design"
              }
            },
            {
              "link": {
                "url": "/services/infrastructure-integrations-governance/",
                "text": "Infrastructure, Integrations & Governance"
              }
            },
            {
              "link": {
                "url": "/services//sales-service-and-crm",
                "text": "Sales, Services & CRM"
              }
            },
            {
              "link": {
                "url": "/services/digital-experience-management",
                "text": "Digital Experience Management"
              }
            },
            {
              "link": {
                "url": "/services/commerce",
                "text": "Commerce"
              }
            },
            {
              "link": {
                "url": "/services/data-analytics-ai",
                "text": "Data, Analytics & AI"
              }
            },
            {
              "link": {
                "url": "/services/marketing-automation",
                "text": "Marketing Automation"
              }
            }
          ]
        }
      ]
    },
    {
      "template": "components/section-col-half",
      "cols": {
        "left": "col-lg-6",
        "right": "col-lg-6"
      },
      "content": {
        "left": [
          {
            "template": "components/component-cards-big-cta",
            "icon": "astrisk",
            "worktype": "Insights",
            "title": "Experience Forward",
            "content": "Building possibilities between customers and brands",
            "link": {
              "url": "https://www.horizontaldigital.com/insights/connected-experiences"
            }
          }
        ],
        "right": [
          {
            "template": "components/component-cards-big-cta",
            "worktype": "Works",
            "title": "PODS",
            "content": "Reimaging the customer and prospect journey",
            "link": {
              "url": "https://www.horizontaldigital.com/work/pods"
            }
          }
        ]
      }
    }
  ]
}
