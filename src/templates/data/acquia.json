{
  "title": "Acquia | Horizontal Digital",
  "meta": {
    "description": "An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle."
  },
  "og": {
    "title":"Acquia | Horizontal Digital",
    "description":"An experience-forward digital consultancy, solving CX challenges across the full customer lifecycle.",
    "url":"https://horizontaldigital.com/platforms",
    "sitename":"Horizontal Digital",
    "img":{
      "src":"https://hztl-fed.azureedge.net/hztl/images/HZTL_SocialImage.png"
    }
  },
  "components": [
    {
      "template":"components/section-wide-media",
      "class":"collapse-bottom-margin",
      "imgs":[
        {
          "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/AcquiaHero.png",
          "alt":"Acquia"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"Harnessing open-source flexibility to create more possibilities",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>Our extensive knowledge of Drupal drives unparalleled enterprise experiences, services and support across the Acquia platform — including Acquia DXP. In addition, we fully leverage the possibilities of Drupal Cloud and Marketing Cloud to help organizations build and deploy content efficiently.</p><p>Plus, we constantly invest in learning and contributing to the Drupal community — enabling us to constantly apply new ideas and advantages to your business.</p>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "title":"Centralized content creation, multiple endpoints",
      "content": [
        {
          "template":"components/rte",
          "content":"<p>The Drupal platform offers customers turnkey options for traditional CMS, decoupled CMS or hybrid CMS for cross channel delivery of content. Acquia’s Drupal Cloud is a Drupal-tuned environment complete with infrastructure, monitoring, and support for reliability and scalability.</p><p>We utilize the breadth of these capabilities to provide innovative business solutions, including:</p><ul><li>Multi-site implementations with shared authoring workflows and content portability</li><li>Multi-region and multi-lingual support for both manual and automated translation service and region-specific content</li><li>Integration with Marketing Cloud and other customer insights and reporting platforms</li><li>Migrate content from an older version of Drupal or your legacy CMS</li></ul>"
        }
      ]
    },
    {
      "template":"components/section-col-2-3",
      "class":"medium-bottom-margin theme-dark",
      "title":"Focus on our users at every touchpoint",
      "content": [
        {
          "template":"components/rte",
          "content":"<h3 class=\"display-1\">We help organizations....</h3><p>We help organizations harness Acquia Marketing Cloud to unlock the power of customer insights. Acquia has focused on integration and speeding up the time to market with a no-code approach to personalization.</p><ul><li>Continuously updated profile for insights and next actions</li><li>No-code personalization and A/B testing campaigns</li><li>Leverage customer profile data for cross-channel engagement</li><li>Visualize and orchestrate the customer journey across all digital touchpoints</li></ul>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"collapse-bottom-margin",
      "title":"Our expertise",
      "content": [
        {
          "template":"components/rte",
          "content":"<h3 class=\"display-2\">Acquia DAM</h3><p>Our Acquia DAM expertise directly connects designers and digital marketers with Acquia’s content marketing capabilities.</p><p>In addition to out-of-the-box setup and configuration, we are proficient with custom DAM integration.</p><p>We developed the Acquia DAM Asset Import module in response to a challenge that called for indexing of DAM content not yet associated with a Drupal content entity.</p><h3 class=\"display-2\">Acquia Site Studio</h3><p>Yesterday’s content marketers talked about pages — today’s talk is about components.</p><p>Layout Builder is a straightforward, visual way to assemble your content into pages whose componentized structure is flexible enough to support multiple types of marketing messages.</p><p>Our fluency across the spectrum marketing platforms allows us to help organize and migrate your legacy content and prepare it for component usage.</p><h3 class=\"display-2\">Acquia personalization</h3><p>We provide deep expertise with Acquia Personalization, enabling component level page personalization, A/B testing, content syndication, and customer insights.</p><p>Our in-house team’s complete knowledge of content strategy and taxonomy combined with Acquia’s powerful insights and segmentation capabilities enable the right content to get to your customers when they need it.</p><h3 class=\"display-2\">Content Migration</h3><p>Drupal is an ideal solution for migrating content from a legacy CMS. We have guided customers through the migration of unstructured content from a multitude of legacy systems into a reusable, componentized marketing platform.</p><p>Over 60% of Drupal sites are running version 7 (end of life extended to November, 2021). Our methodical approach to content migration is ideally suited to help in your upgrade to version 9, where you can unlock the power of a modern decoupled architecture, improved tooling for deployments and dependency management, and an easy to use interface for marketers and content authors.</p>"
        }
      ]
    },
    {
      "template":"components/section-col-single",
      "class":"medium-bottom-margin",
      "content": [
        {
          "template":"components/component-team",
          "title":"Our Team",
          "items": [
            {
              "name":"Jeffrey Isham",
              "title":"Practice Lead, Open & Emerging Technologies",
              "linkedin":"https://www.linkedin.com/in/jeffreyisham/",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/acquia/team/JeffreyIsham.png"
              }
            },
            {
              "name":"Song Vang",
              "title":"Techology Lead",
              "linkedin":"https://www.linkedin.com/in/songvang/",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/acquia/team/SongVang.png"
              }
            },
            {
              "name":"Jeremiah Davis",
              "title":"Technology Lead",
              "linkedin":"https://www.linkedin.com/in/jeremiah-davis-4947b44/",
              "img":{
                "src":"https://hztl-fed.azureedge.net/hztl/images/platforms/acquia/team/JeremiahDavis.png"
              }
            }
          ]
        }
      ]
    }
  ]
}
