;(function($){

  var Navigation = function($elem){

    var config = {
      selectors: {
        button:'.navbar-toggle'
      },
      classes: {
        collapsed:'collapsed',
        navOpen:'site-nav-open',
        bodyNavOpen:'body-nav-open'
      }
    }

    var $btn,$header,$body;

    /*
     * Listeners
     */

    var handleToggle = function(e){

      e.preventDefault();

      var $this = $(this);

      if($this.hasClass(config.classes.collapsed)) {
        $this.removeClass(config.classes.collapsed);
        $header.addClass(config.classes.navOpen);
        $body.addClass(config.classes.bodyNavOpen);
      } else {
        $this.addClass(config.classes.collapsed);
        $header.removeClass(config.classes.navOpen);
        $body.removeClass(config.classes.bodyNavOpen);
      }

    }

    /*
     * Setup Funcitons
     */

    var createScrollTrigger = function(){

      var element = $header[0];

      ScrollTrigger.create({
        trigger: "body",
        start: "top top",
        end: "bottom bottom",
        onUpdate: self => {
          if(self.direction > 0 && !element.classList.contains('slideUp'))  {
            element.classList.add('slideUp');
          } else if(self.direction < 0 && element.classList.contains('slideUp')) {
            element.classList.remove('slideUp');
          }
        }
      });
    }

    /*
     * Utility
     */



    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $body = $('body');
      $header = $elem;
      $btn = $(config.selectors.button,$header);
    }

    var setUp = function(){
      createScrollTrigger();
    }

    var attachListeners = function(){
      $btn.on('click',handleToggle);
    }

    return init();

  }

  $(document).ready(function(){
    $('.hztl-header').each(function(){
      new Navigation(  $(this) );
    });
  });

})(jQuery);
