;(function($){

  var LogoFilters = function($elem){

    var config = {
      selectors: {
        filters:'.logo-filters',
        grid:'.logo-grid',
        item:'.grid-item',
        link:'a'
      },
      classes: {
        active:'active'
      }
    }

    var $section,$nav,$btns,$grid,$items;

    /*
     * Listeners
     */

    var handleFilter = function(e){

      e.preventDefault();

      var $this = $(this);

      $btns.removeClass(config.classes.active);
      $this.addClass(config.classes.active);

      var filter = $this.attr('data-filter');

      $items.hide();
      filterBy(filter);

    }

    /*
     * Setup Funcitons
     */


    /*
     * Utility
     */

    var filterBy = function(filter){

      $items.each(function(){
        var filters = $(this).attr('data-tags');
        if(filters.includes(filter)) {
          $(this).show();
        }
      });

    }

    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $section = $elem;
      $nav = $(config.selectors.filters,$section);
      $grid = $(config.selectors.grid,$section);
      $btns = $(config.selectors.link,$nav);
      $items = $(config.selectors.item,$grid);
    }

    var setUp = function(){
      filterBy('Featured');
    }

    var attachListeners = function(){
      $btns.on('click',handleFilter);
    }

    return init();

  }

  $(document).ready(function(){
    $('.section-logos').each(function(){
      new LogoFilters(  $(this) );
    });
  });

})(jQuery);
