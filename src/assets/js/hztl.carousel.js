;(function($){

  var HztlCarousel = function($elem){

    var config = {
      selectors: {},
      classes: {}
    }

    var $container;

    /*
     * Listeners
     */



    /*
     * Setup Funcitons
     */



    /*
     * Utility
     */



    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
    }

    var setUp = function(){
      $elem.slick(
        {
          "autoplaySpeed":4000,
          "autoplay":true,
          "isPauseEnabled":true,
          "lazyLoad":"ondemand",
          "dots": true,
          "fade": true,
          "infinite": true,
          "arrows":false,
          "rows": 0
        }
      );
    }

    var attachListeners = function(){}

    return init();

  }

  $(document).ready(function(){
    $('.carousel').each(function(){
      new HztlCarousel( $(this) );
    });
  });

})(jQuery);
