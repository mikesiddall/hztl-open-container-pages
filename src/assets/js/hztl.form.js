;(function($){

  var Form = function($elem){

    var config = {
      selectors: {
        formrow:'.hztl-form-row',
        input:'.hztl-form-text-input-container input, .hztl-form-text-input',
        label:'.hztl-form-text-label'
      }
    }

    var $container,$inputs;

    /*
     * Listeners
     */

    var handleFocus = function(e){
      var $this = $(this);
      $this.css('background', "#2f2d2e");
    }

    var handleBlur = function(e){
      var $this = $(this);
      if($this.val().length > 0) {
        $this.css('background', "#2f2d2e");
      } else {
        $this.css('background', "rgba(0, 0, 0, 0)");
      }
    }

    /*
     * Setup Funcitons
     */



    /*
     * Utility
     */



    /*
     * INIT
     */

    var init = function(){
      getElements();
      setUp();
      attachListeners();
    }

    var getElements = function(){
      $container = $elem;
      $inputs = $(config.selectors.input,$container);
    }

    var setUp = function(){
      $inputs.each(handleBlur);
      $inputs.each(function(e){
        if($(this).closest(config.selectors.formrow).hasClass('error')) {
          $(document).scrollTop( $container.offset().top );
        }
      })
    }

    var attachListeners = function(){
      $inputs.on("focus",handleFocus);
      $inputs.on("blur",handleBlur);
      $inputs.on("input",handleBlur);
    }

    return init();

  }

  $(document).ready(function(){
    $('.hztl-form').each(function(){
      new Form(  $(this) );
    });
  });

})(jQuery);
