$('.lazy-img').Lazy({
  'beforeLoad':function($elem){
    $elem.addClass('lazy-loading');
  },
  'afterLoad':function($elem){
    $elem.removeClass('lazy-loading').addClass('lazy-loaded');
  }
});
