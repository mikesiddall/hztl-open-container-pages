const gulp = require('gulp');
const gulpIf = require('gulp-if');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const htmlmin = require('gulp-htmlmin');
const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const hb = require('gulp-hb');
const clean = require('gulp-clean');
const rename = require('gulp-rename');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');

const htmlFilesArr = ['src/**/*.html'];

const cssFilesArr = [
  'src/assets/sass/style.scss'
];

const jsFilesArr = [
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/jquery-lazy/jquery.lazy.min.js',
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/slick-slider/slick/slick.min.js',
  'node_modules/@vimeo/player/dist/player.min.js',
  'node_modules/gsap/dist/gsap.min.js',
  'node_modules/gsap/dist/ScrollTrigger.min.js',
  'src/assets/js/**/*.js',
];

function html() {
  return gulp
    .src(htmlFilesArr)
    .pipe(
      hb()
        .partials('./src/templates/partials/**/*.hbs')
        .helpers('./src/templates/helpers/*.js')
        .data('./src/templates/data/**/*.{js,json}'),
    )
    .pipe(gulp.dest('./dist'));
}

function css() {
  return gulp
    .src(cssFilesArr)
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        includePaths: ['node_modules'],
      }).on('error', sass.logError),
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/assets/css/'));
}

function cssprod() {
  return gulp
    .src(cssFilesArr)
    .pipe(
      sass({
        includePaths: ['node_modules'],
      }).on('error', sass.logError),
    )
    .pipe(cssmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/assets/css/'));
}

function fonts() {
  return gulp
    .src('src/assets/fonts/**/*.*')
    .pipe(gulp.dest('dist/assets/fonts/'));
}

function icons() {
  var fontName = 'icons';
  return gulp
    .src(['src/assets/icons/*.svg'])
    .pipe(
      iconfontCss({
        fontName: fontName,
        targetPath: '../sass/_icons.scss',
        fontPath: '../fonts/',
      }),
    )
    .pipe(
      iconfont({
        fontName: fontName,
        // Remove woff2 if you get an ext error on compile
        // formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
        formats: ['svg', 'woff2'],
        normalize: true,
        fontHeight: 1001,
      }),
    )
    .pipe(gulp.dest('src/assets/fonts/'));
}

function js() {
  return gulp
    .src(jsFilesArr, { allowEmpty: true })
    /*.pipe(
      babel({
        presets: ['@babel/preset-env'],
      }),
    )*/
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/assets/js'));
}

function jsprod() {
  return gulp
    .src(jsFilesArr, { allowEmpty: true })
    /*.pipe(
      babel({
        presets: ['@babel/preset-env'],
      }),
    )*/
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('dist/assets/js'));
}

function img() {
  return gulp
    .src('src/assets/images/**/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/assets/images/'));
}

function serve() {
  browserSync.init({
    open: true,
    server: './dist',
  });
}

function browserSyncReload(done) {
  browserSync.reload();
  done();
}

function watchFiles() {
  gulp.watch('src/**/*.html', gulp.series(html, browserSyncReload));
  gulp.watch('src/**/*.hbs', gulp.series(html, browserSyncReload));
  gulp.watch('src/**/*.json', gulp.series(html, browserSyncReload));
  gulp.watch('src/**/*.scss', gulp.series(css, browserSyncReload));
  gulp.watch('src/**/*.js', gulp.series(js, browserSyncReload));
  gulp.watch('src/img/**/*.*', gulp.series(img));

  return;
}

function del() {
  return gulp.src('dist/*', { read: false }).pipe(clean());
}

function staticCss() {
  return gulp
    .src('src/assets/sass/static.scss')
    .pipe(
      sass({
        includePaths: ['node_modules'],
      }).on('error', sass.logError),
    )
    .pipe(rename('unity-style.css'))
    .pipe(gulp.dest('dist/static/css/'));
}

function staticImg() {
  return gulp
    .src([
      'src/assets/images/unity-logo_dark.svg',
      'src/assets/images/unity-favicon.ico',
      'src/assets/images/HR_Unity_ID-wHRandTM-NEG.png',
    ])
    .pipe(imagemin())
    .pipe(gulp.dest('dist/static/img/'));
}

exports.del = del;

exports.serve = gulp.parallel(
  html,
  css,
  fonts,
  js,
  img,
  watchFiles,
  serve,
);
exports.prod = gulp.series(
  del,
  html,
  css,
  cssprod,
  fonts,
  js,
  jsprod,
  img,
);
exports.icons = gulp.series(icons);
exports.static = gulp.series(staticCss, staticImg);
exports.default = gulp.series(
  del,
  html,
  css,
  cssprod,
  fonts,
  js,
  jsprod,
  img,
);
